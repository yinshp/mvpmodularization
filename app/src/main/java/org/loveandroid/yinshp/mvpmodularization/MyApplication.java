package org.loveandroid.yinshp.mvpmodularization;

import com.alibaba.android.arouter.launcher.ARouter;

import org.loveandroid.yinshp.lib_common.base.BaseApplication;
import org.loveandroid.yinshp.lib_common.utils.AppUtil;

/**
 * Author : yinshengpan
 * Email : yinshp0423@163.com
 * Created date :　18-2-23 上午10:51
 * Describe :
 */
public class MyApplication extends BaseApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        //初始化ARout
        if (AppUtil.isAppDebug()) {
            //开启InstantRun之后，一定要在ARouter.init之前调用openDebug
            ARouter.openDebug();
            ARouter.openLog();
        }
        ARouter.init(this);
    }
}
