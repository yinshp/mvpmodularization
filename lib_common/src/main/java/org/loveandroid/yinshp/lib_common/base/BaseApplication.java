package org.loveandroid.yinshp.lib_common.base;

import android.app.Application;
import android.content.Context;

/**
 * =========================================
 * Author : yinshengpan
 * Email : yinshp0423@163.com
 * Created date : 18-2-22 下午7:18
 * Describe :
 * 要想使用BaseApplication，必须在组件中实现自己的Application，并且继承BaseApplication；
 * 组件中实现的Application必须在debug包中的AndroidManifest.xml中注册，否则无法使用；
 * 组件的Application需置于java/debug文件夹中，不得放于主代码；
 * ==========================================
 */
public class BaseApplication extends Application {

    private static BaseApplication mInstance;

    /**
     * 全局Context
     */
    public Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance=this;
        context=this.getApplicationContext();
    }

    /**
     * 获取BaseApplication实例
     * @return
     */
    public static BaseApplication getIns(){
        return mInstance;
    }

}
