package org.loveandroid.yinshp.lib_common.bean;

/**
 * Author : yinshengpan
 * Email : yinshp0423@163.com
 * Created date :　18-2-23 下午4:04
 * Describe :
 */
public class ErrorBean {


    /**
     * code : -10001007
     * message : 当前消息为空
     */
    public String code;
    public String message;

    @Override
    public String toString() {
        return "ErrorBean{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
