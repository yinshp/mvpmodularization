package org.loveandroid.yinshp.lib_common.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * =========================================
 * Author : yinshengpan
 * Email : yinshp0423@163.com
 * Created date : 18-2-23 下午4:06
 * Describe :
 * ==========================================
 */
public class LoginResult{

    /**
     * code : 1
     * message : [{"user_id":"42691","user_account":"","user_phone":"17673641423","user_pwd":"9b2ee6cd1d765c4cc390c6deb3bf0bfb","user_weixin":"yinshp","user_qq":"631549029","user_reg_time":"2017-09-04 10:14:30","user_login_time":"2017-11-15 20:31:02","user_state":"1"}]
     * nick :
     */

    public String code;
    public String nick;
    public List<MessageBean> message;

    public static class MessageBean{
        /**
             * user_id : 42691
         * user_account :
         * user_phone : 17673641423
         * user_pwd : 9b2ee6cd1d765c4cc390c6deb3bf0bfb
         * user_weixin : yinshp
         * user_qq : 631549029
         * user_reg_time : 2017-09-04 10:14:30
         * user_login_time : 2017-11-15 20:31:02
         * user_state : 1
         */

        @SerializedName("user_id")
        public String userId;
        @SerializedName("user_account")
        public String userAccount;
        @SerializedName("user_phone")
        public String userPhone;
        @SerializedName("user_pwd")
        public String userPwd;
        @SerializedName("user_weixin")
        public String userWeixin;
        @SerializedName("user_qq")
        public String userQq;
        @SerializedName("user_reg_time")
        public String userRegTime;
        @SerializedName("user_login_time")
        public String userLoginTime;
        @SerializedName("user_state")
        public String userState;

        @Override
        public String toString() {
            return "MessageBean{" +
                    "userId='" + userId + '\'' +
                    ", userAccount='" + userAccount + '\'' +
                    ", userPhone='" + userPhone + '\'' +
                    ", userPwd='" + userPwd + '\'' +
                    ", userWeixin='" + userWeixin + '\'' +
                    ", userQq='" + userQq + '\'' +
                    ", userRegTime='" + userRegTime + '\'' +
                    ", userLoginTime='" + userLoginTime + '\'' +
                    ", userState='" + userState + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "LoginResult{" +
                "code='" + code + '\'' +
                ", nick='" + nick + '\'' +
                ", message=" + message.toString() +
                '}';
    }
}
