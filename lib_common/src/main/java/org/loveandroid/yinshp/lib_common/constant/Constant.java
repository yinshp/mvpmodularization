package org.loveandroid.yinshp.lib_common.constant;

/**
 * Author : yinshengpan
 * Email : yinshp0423@163.com
 * Created date :　18-2-23 上午8:40
 * Describe : 常量类
 */
public class Constant {


    /**
     * 正则表达式之效验手机号码
     * 大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数
     * 此方法中前三位格式有：
     * 13+任意数
     * 15+除4的任意数
     * 18+除1和4的任意数
     * 17+除9的任意数
     * 147
     */
    public static final String PHONE_NUMBER_REG_EXP = "^((13[0-9])|(15[^4])|(18[0,2,3,5-9])|(17[0-8])|(147))\\d{8}$";


    /**
     * 正则表达式之效验微信号
     * 微信号规则：微信账号仅支持6-20个字母、数字、下划线或减号，以字母开头。
     * 解释一下，只要是字母开头，可以是纯字母（hjqzHJQZhongjiqiezi），或字母数字混合，或。。。。
     */
    public static final String WEIXIN_NUMBER_REG_EXP = "^[a-zA-Z]{1}([a-zA-Z0-9]|[_-]){5,19}$";

    /**
     * 正则表达式之效验密码
     * 密码由字母、数字、“_”、“.”组成的6到16位的字符串
     */
    public static final String PASSWORD_REG_EXP = "^([a-zA-Z0-9]|[_.]){6,16}$";


    /**
     * android型号ID
     */
    public static String androidID;

    /**
     * androidIM
     */
    public static String androidIMEI;

}
