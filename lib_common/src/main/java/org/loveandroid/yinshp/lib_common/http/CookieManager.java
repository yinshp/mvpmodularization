package org.loveandroid.yinshp.lib_common.http;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

/**
 * =========================================
 * Author : yinshengpan
 * Email : yinshp0423@163.com
 * Created date : 18-2-23 上午10:13
 * Describe : cookie管理器
 * ==========================================
 */
public class CookieManager implements CookieJar {

    private Map<String, List<Cookie>> cookJars = new HashMap<>();

    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
        cookJars.put(url.host(), cookies);
    }

    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {
        return cookJars.get(url.host()) == null ? new ArrayList<Cookie>() : cookJars.get(url.host());
    }
}
