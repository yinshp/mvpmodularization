package org.loveandroid.yinshp.lib_common.http;



/**
 * =========================================
 * Author : yinshengpan
 * Email : yinshp0423@163.com
 * Created date : 18-2-23 上午10:14
 * Describe :  数据处理回调
 * ==========================================
 */
public interface MyCallBack<T> {

    /**
     * 网络请求失败的回调
     * @param msg 提示信息
     * @param e 异常信息
     */
    void onFail(String msg, Exception e);

    /**
     *  网络请求成功的回调
     * @param t 回调的对象数据
     */
    void onSucceed(T t);
}
