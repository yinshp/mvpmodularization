package org.loveandroid.yinshp.lib_common.utils;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import org.loveandroid.yinshp.lib_common.base.BaseApplication;

/**
 * Author : yinshengpan
 * Email : yinshp0423@163.com
 * Created date :　18-2-23 上午11:09
 * Describe :
 */
public class AppUtil {

    /**
     * 判断App是否是Debug版本
     *
     * @return {@code true}: 是<br>{@code false}: 否
     */
    public static boolean isAppDebug() {
            if (StringUtil.isSpace(BaseApplication.getIns().context.getPackageName())) {
            return false;
        }
        try {
            PackageManager pm = BaseApplication.getIns().context.getPackageManager();
            ApplicationInfo ai = pm.getApplicationInfo(BaseApplication.getIns().context.getPackageName(), 0);
            return ai != null && (ai.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }
}
