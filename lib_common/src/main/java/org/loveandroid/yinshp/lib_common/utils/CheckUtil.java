package org.loveandroid.yinshp.lib_common.utils;

import android.text.TextUtils;

import org.loveandroid.yinshp.lib_common.constant.Constant;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Author : yinshengpan
 * Email : yinshp0423@163.com
 * Created date :　18-2-23 上午10:08
 * Describe : 校验工具类
 */
public class CheckUtil {
    /**
     * 效验手机号码
     *
     * @param phone 手机号码
     * @return 是否正确
     * @throws PatternSyntaxException 异常信息
     */
    public static boolean isChinaPhoneLegal(String phone) throws PatternSyntaxException {
        if (!TextUtils.isEmpty(phone)) {
            Pattern p = Pattern.compile(Constant.PHONE_NUMBER_REG_EXP);
            Matcher m = p.matcher(phone);
            return m.matches();
        }
        return false;

    }


    /**
     * 效验微信号
     *
     * @param weixin 微信号
     * @return 是否正确
     * @throws PatternSyntaxException 异常信息
     */
    public static boolean isWeiXinLegal(String weixin) throws PatternSyntaxException {
        if (!TextUtils.isEmpty(weixin)) {
            Pattern p = Pattern.compile(Constant.WEIXIN_NUMBER_REG_EXP);
            Matcher m = p.matcher(weixin);
            return m.matches();
        }
        return false;
    }

    /**
     * 效验密码
     *
     * @param password 密码
     * @return 是否正确
     * @throws PatternSyntaxException 异常信息
     */
    public static boolean isPasswordLegal(String password) throws PatternSyntaxException {
        if (!TextUtils.isEmpty(password)) {
            Pattern p = Pattern.compile(Constant.PASSWORD_REG_EXP);
            Matcher m = p.matcher(password);
            return m.matches();
        }
        return false;
    }

}
