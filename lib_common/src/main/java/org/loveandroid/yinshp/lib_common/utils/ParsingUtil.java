package org.loveandroid.yinshp.lib_common.utils;

import com.google.gson.Gson;

/**
 * Author : yinshengpan
 * Email : yinshp0423@163.com
 * Created date :　18-2-23 上午9:51
 * Describe : 解析工具类
 */
public class ParsingUtil {

    private static Gson gson=new Gson();

    /**
     * Gson解析封装
     * @param json json数据
     * @param clazz 对象实体类
     * @param <T> 返回的对象
     * @return
     */
    public static <T> T parsingData(String json,Class<T> clazz){
        return gson.fromJson(json,clazz);
    }
}
