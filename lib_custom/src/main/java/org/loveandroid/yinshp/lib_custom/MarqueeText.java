package org.loveandroid.yinshp.lib_custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author : yinshengpan
 * Email : yinshp0423@163.com
 * Created date 2017/10/12
 * 跑马灯自定义View
 */
public class MarqueeText extends View {

    private static final String TAG = "MarqueeText";
    /**
     * 开始绘制水平文本的X，Y坐标
     */
    private int startHorizontalDrawX = 0;
    private float startHorizontalDrawY = 0;

    /**
     * 开始绘制垂直文本的X，Y坐标
     */
    private int startVerticalDrawX = 0;
    private float startVerticalDrawY = 0;


    private Paint mPaint;

    /**
     * 文本的宽度与高度
     */
    private int textWidth;
    private int textHeight;

    /**
     * 文本是否超过文本框
     */
    private boolean isOutSide;

    /**
     * 要绘制的文本
     */
    public String drawTextOne;
    public String drawTextTwo;

    public int textColor;
    public float textSize;
    public int speed;
    /**
     * 设置长文字空白连接
     */
    public int textBlank;

    /**
     * horizontal  默认值  代表水平   vertical 代表垂直
     */
    public String orientation;

    /**
     * 垂直停留时间
     */
    private int verticalTime;

    /**
     * 显示的行数
     * NUM_LINE_ONE 代表显示一行
     * NUM_LINE_TWO 代表显示两行
     */
    private int numLine;
    public final int NUM_LINE_ONE = 1;
    public final int NUM_LINE_TWO = 2;


    /**
     * java 代码中使用的构造函数
     *
     * @param context
     */
    public MarqueeText(Context context) {
        super(context);
        initView();
    }


    /**
     * XML文件中使用
     *
     * @param context
     * @param attrs
     */
    public MarqueeText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);


        if (attrs != null) {
            //从XML文件
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.MarqueeText);
            textColor = array.getColor(R.styleable.MarqueeText_marquee_TextColor, Color.BLACK);
            textSize = array.getDimension(R.styleable.MarqueeText_marquee_TextSize, sp2px(30));
            speed = array.getInteger(R.styleable.MarqueeText_marquee_ScrollingSpeed, 5);
            drawTextOne = array.getString(R.styleable.MarqueeText_marquee_TextOne);
            drawTextTwo = array.getString(R.styleable.MarqueeText_marquee_TextTwo);
            if (drawTextTwo == null) {
                drawTextTwo = drawTextOne;
            }
            orientation = array.getString(R.styleable.MarqueeText_marquee_Orientation);
            if (orientation == null) {
                orientation = getContext().getString(R.string.orientation_horizontal);
            }
            textBlank = array.getInt(R.styleable.MarqueeText_marquee_TextBlank, 200);
            verticalTime = array.getInt(R.styleable.MarqueeText_marquee_Vertical_Stop_Time, 2);
            numLine = array.getInt(R.styleable.MarqueeText_marquee_Num_Line, NUM_LINE_ONE);
            array.recycle();
        }

        initView();
    }

    private void initView() {

        Paint p = new Paint();
        p.setColor(textColor);
        p.setTextSize(sp2px(textSize));
        p.setAntiAlias(true);
        mPaint = p;
        getTextWH();
    }

    /**
     * 获取文本的宽高度
     */
    private void getTextWH() {
        if (drawTextOne != null) {
            Rect rect = new Rect();
            if (drawTextTwo != null) {
                if (drawTextOne.length() > drawTextTwo.length()) {
                    mPaint.getTextBounds(drawTextOne, 0, drawTextOne.length(), rect);
                } else {
                    mPaint.getTextBounds(drawTextTwo, 0, drawTextTwo.length(), rect);
                }
            } else {
                mPaint.getTextBounds(drawTextOne, 0, drawTextOne.length(), rect);
            }
            //通过一个把文字包裹起来的矩形框，来获取文本的宽高；
            textHeight = rect.height();
            textWidth = rect.width();
        }


    }

    /**
     * 测量View的大小
     *
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //测量宽度
        int width = getMySize(textWidth, widthMeasureSpec);

        //测量高度
        int height = getMySize(textHeight, heightMeasureSpec);

        //设置宽高，一定要设置，否则没有效果
        setMeasuredDimension(width, height);


    }


    /**
     * @param canvas
     */
    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        //获取文本宽高
        getTextWH();

        //判断显示行数是否小于等于2，否则显示一行
        if (numLine > NUM_LINE_TWO) {
            numLine = NUM_LINE_ONE;
        }

        //判断文本宽度是否超出布局宽度
        if (textWidth >= getMeasuredWidth()) {
            isOutSide = true;
        } else {
            isOutSide = false;
        }

        //垂直滚动
        if (getContext().getString(R.string.orientation_vertical).equals(orientation)) {
            if (isOutSide) {
                //文本长度超出文本框的情况
                getVerticalTextLenghtBeyondTextBox(canvas);
            } else {
                //文本长度未超出文本框的情况
                getVerticalTextLenghtNotBeyondTextBox(canvas);
            }
            //水平滚动
        } else if (getContext().getString(R.string.orientation_horizontal).equals(orientation)) {
            if (isOutSide) {
                //文本长度超出文本框的情况
                getHorizontalTextLenghtBeyondTextBox(canvas);
            } else {
                //文本长度未超出文本框的情况
                getHorizontalTextLenghtNotBeyondTextBox(canvas);
            }
        }
    }

    /**
     * 文字超过文本框的垂直滚动
     *
     * @param canvas
     */
    private void getVerticalTextLenghtBeyondTextBox(Canvas canvas) {
        //当文本全部滚动出控件
        if (startVerticalDrawY >= (getMeasuredHeight() + textHeight * numLine)) {
            startVerticalDrawY = textHeight * numLine;
            startVerticalDrawX = 0;
        }
        //文本超出区域
        float outSide = startVerticalDrawY;
        //绘制超出区域的文本
        if (outSide > getMeasuredHeight()) {
            if (numLine == NUM_LINE_ONE) {
                canvas.drawText(drawTextOne, 0, outSide - getMeasuredHeight(), mPaint);
            } else if (numLine == NUM_LINE_TWO) {
                canvas.drawText(drawTextOne, 0, outSide - getMeasuredHeight(), mPaint);
                canvas.drawText(drawTextTwo, 0, outSide - getMeasuredHeight() - textHeight, mPaint);
            }
            postInvalidateDelayed(10);
        }
        //居中的基线位置
        float centerH = getCenterBaseline();
        //是否到达基线位置
        if (startVerticalDrawY >= centerH && startVerticalDrawY < (centerH + speed)) {
            startVerticalDrawY = centerH;
            startVerticalDrawX -= speed;
            //判断文本是否已经显示完
            if (startVerticalDrawX <= (getMeasuredWidth() - textWidth)) {
                startVerticalDrawY += speed;
            }

        } else {
            startVerticalDrawY += speed;
        }

        if (numLine == NUM_LINE_ONE) {
            canvas.drawText(drawTextOne, startVerticalDrawX, startVerticalDrawY, mPaint);
        } else if (numLine == NUM_LINE_TWO) {
            canvas.drawText(drawTextOne, startVerticalDrawX, startVerticalDrawY, mPaint);
            canvas.drawText(drawTextTwo, startVerticalDrawX, startVerticalDrawY - textHeight, mPaint);
        }

        postInvalidateDelayed(30);
    }


    /**
     * 文字不超过文本框的垂直滚动
     *
     * @param canvas
     */
    private void getVerticalTextLenghtNotBeyondTextBox(Canvas canvas) {
        //当文本全部滚动出控件
        if (startVerticalDrawY > (getMeasuredHeight() + textHeight * numLine)) {
            startVerticalDrawY = textHeight * numLine;
        }

        //文本超出区域
        float outSide = startVerticalDrawY;

        //获取水平居中位置
        startVerticalDrawX = (getMeasuredWidth() - textWidth) / 2;

        //绘制超出区域的文本
        if (outSide >= getMeasuredHeight()) {

            if (numLine == NUM_LINE_ONE) {
                canvas.drawText(drawTextOne, startVerticalDrawX, outSide - getMeasuredHeight(), mPaint);
            } else if (numLine == NUM_LINE_TWO) {
                canvas.drawText(drawTextOne, startVerticalDrawX, outSide - getMeasuredHeight(), mPaint);
                canvas.drawText(drawTextTwo, startVerticalDrawX, outSide - getMeasuredHeight() - textHeight, mPaint);
            }

        }

        //垂直滚动居中的基线
        float centerH = getCenterBaseline();

        if (startVerticalDrawY >= centerH && startVerticalDrawY < (centerH + speed)) {

            //绘制居中显示  停顿
            if (numLine == NUM_LINE_ONE) {
                canvas.drawText(drawTextOne, startVerticalDrawX, centerH, mPaint);
            } else if (numLine == NUM_LINE_TWO) {
                canvas.drawText(drawTextOne, startVerticalDrawX, centerH, mPaint);
                canvas.drawText(drawTextTwo, startVerticalDrawX, centerH - textHeight, mPaint);
            }

            postInvalidateDelayed(verticalTime * 1000);

        } else {

            //绘制文本
            if (numLine == NUM_LINE_ONE) {
                canvas.drawText(drawTextOne, startVerticalDrawX, startVerticalDrawY, mPaint);
            } else if (numLine == NUM_LINE_TWO) {
                canvas.drawText(drawTextOne, startVerticalDrawX, startVerticalDrawY, mPaint);
                canvas.drawText(drawTextTwo, startVerticalDrawX, startVerticalDrawY - textHeight, mPaint);
            }

            postInvalidateDelayed(10);

        }
        startVerticalDrawY += speed;
    }

    /**
     * 获取垂直滚动的基线
     *
     * @return
     */
    private float getCenterBaseline() {
        return getMeasuredHeight() / 2 + numLine * (mPaint.descent() - mPaint.ascent()) / 2 - mPaint.descent();
    }

    /**
     * 文本长度超出文本框水平滚动的绘制方法
     *
     * @param canvas
     */
    private void getHorizontalTextLenghtBeyondTextBox(Canvas canvas) {
        //判断文本是否超出区域
        if (startHorizontalDrawX < -textWidth) {
            startHorizontalDrawX = textBlank;
        }

        //获取垂直居中的基线
        startHorizontalDrawY = getCenterBaseline();

        //文本超出区域
        int outSide = startHorizontalDrawX;

        //绘制超出区域的文本
        if (outSide < -(textWidth - getMeasuredWidth())) {

            if (numLine == NUM_LINE_ONE) {
                canvas.drawText(drawTextOne, textWidth + outSide + textBlank, startHorizontalDrawY, mPaint);
            } else if (numLine == NUM_LINE_TWO) {
                canvas.drawText(drawTextOne, textWidth + outSide + textBlank, startHorizontalDrawY, mPaint);
                canvas.drawText(drawTextTwo, textWidth + outSide + textBlank, startHorizontalDrawY - textHeight, mPaint);
            }

        }
        //绘制文本
        if (numLine == NUM_LINE_ONE) {
            canvas.drawText(drawTextOne, startHorizontalDrawX, startHorizontalDrawY, mPaint);
        } else if (numLine == NUM_LINE_TWO) {
            canvas.drawText(drawTextOne, startHorizontalDrawX, startHorizontalDrawY, mPaint);
            canvas.drawText(drawTextTwo, startHorizontalDrawX, startHorizontalDrawY - textHeight, mPaint);
        }

        postInvalidateDelayed(10);
        startHorizontalDrawX -= speed;
    }

    /**
     * 文本长度未超出文本框水平滚动的绘制方法
     *
     * @param canvas
     */
    private void getHorizontalTextLenghtNotBeyondTextBox(Canvas canvas) {
        //判断文本是否超出区域
        if (startHorizontalDrawX < -textWidth) {

            startHorizontalDrawX = getMeasuredWidth() - textWidth;
        }

        //获取垂直居中的基线
        startHorizontalDrawY = getCenterBaseline();

        int outSide = startHorizontalDrawX;
        //超出区域，在右边绘制超出的部分
        if (outSide < 0) {

            if (numLine == NUM_LINE_ONE) {
                canvas.drawText(drawTextOne, getMeasuredWidth() + outSide, startHorizontalDrawY, mPaint);
            } else if (numLine == NUM_LINE_TWO) {
                canvas.drawText(drawTextOne, getMeasuredWidth() + outSide, startHorizontalDrawY, mPaint);
                canvas.drawText(drawTextTwo, getMeasuredWidth() + outSide, startHorizontalDrawY - textHeight, mPaint);
            }

        }

        //绘制文本
        if (numLine == NUM_LINE_ONE) {
            canvas.drawText(drawTextOne, startHorizontalDrawX, startHorizontalDrawY, mPaint);
        } else if (numLine == NUM_LINE_TWO) {
            canvas.drawText(drawTextOne, startHorizontalDrawX, startHorizontalDrawY, mPaint);
            canvas.drawText(drawTextTwo, startHorizontalDrawX, startHorizontalDrawY - textHeight, mPaint);
        }

        //10ms刷新一次界面
        postInvalidateDelayed(10);
        //X的坐标向左移动达到移动的效果
        startHorizontalDrawX -= speed;
    }


    /**
     * 测量
     *
     * @param defaultSize
     * @param measureSpec
     * @return
     */
    private int getMySize(int defaultSize, int measureSpec) {
        int mySize = defaultSize;
        int mode = MeasureSpec.getMode(measureSpec);
        int size = MeasureSpec.getSize(measureSpec);
        switch (mode) {
            //如果没有指定大小，就设置为默认大小
            case MeasureSpec.UNSPECIFIED: {
                mySize = defaultSize;
//                Log.i(TAG, "getMySize: 未指定");
                break;
            }
            //如果测量模式是最大取值为size
            case MeasureSpec.AT_MOST: {
                /**
                 * WRAP_CONTENT
                 * 我们将大小取最小值,也可以取其他值
                 */
                mySize = Math.min(size, defaultSize);
//                Log.i(TAG, "getMySize: 最大值");
                break;
            }
            case MeasureSpec.EXACTLY: {
                //如果是固定的大小，那就不要去改变它,MatchParent或者明确的数值
                mySize = size;
//                Log.i(TAG, "getMySize: 固定大小");
                break;
            }
            default:
                break;
        }
        return mySize;
    }

    /**
     * sp转px
     */
    private int sp2px(float spValue) {
        float fontScale = getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }


    /**
     * 代码中设置速度的方法
     *
     * @param speed 速度
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * 代码中设置垂直水平的方法
     *
     * @param orientation //默认 horizontal 水平 ， vertical  垂直
     */
    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    /**
     * 用于设置垂直滚动停顿时间
     *
     * @param verticalTime 时间
     */
    public void setVerticalTime(int verticalTime) {
        this.verticalTime = verticalTime;
    }

    /**
     * 设置显示行数
     *
     * @param numLine 行数
     */
    public void setNumLine(int numLine) {
        this.numLine = numLine;
    }


    /**
     * 设置单行显示的文字
     *
     * @param drawTextOne 文本
     */
    public void setDrawTextOne(String drawTextOne) {
        this.drawTextOne = drawTextOne;
        drawTextTwo=null;
        numLine = NUM_LINE_ONE;
        requestLayout();
        invalidate();
    }

    /**
     * 设置单行显示的文字
     *
     * @param drawTextTwo
     */
    public void setDrawTextTwo(String drawTextOne, String drawTextTwo) {
        this.drawTextTwo = drawTextTwo;
        this.drawTextOne = drawTextOne;
        numLine = NUM_LINE_TWO;
        requestLayout();
        invalidate();
    }

    /**
     * 代码中设置文字的颜色
     *
     * @param textColor 文字颜色
     */
    public void setTextColor(int textColor) {
        this.textColor = textColor;
        mPaint.setColor(textColor);
    }

    /**
     * 代码中设置文字的大小
     *
     * @param textSize 文字大小
     */
    public void setTextSize(float textSize) {
        this.textSize = textSize;
        mPaint.setTextSize(sp2px(textSize));
    }

    /**
     * 代码中设置长文字留空白长度
     *
     * @param textBlank 留空白长度
     */
    public void setTextBlank(int textBlank) {
        this.textBlank = textBlank;
        requestLayout();
        invalidate();
    }
}
