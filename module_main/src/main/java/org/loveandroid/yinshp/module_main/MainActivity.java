package org.loveandroid.yinshp.module_main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.loveandroid.yinshp.lib_common.constant.Constant;


import io.reactivex.functions.Consumer;

@Route(path = "/main/main")
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * 点击登录
     */
    private TextView mTvLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

    }

    private void initView() {
        mTvLogin = findViewById(R.id.tv_login);
        mTvLogin.setOnClickListener(this);
        initPermission();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tv_login) {
        }
    }

    /**
     * 获取手机信息权限
     */
    private void initPermission() {
        new RxPermissions(this).request(Manifest.permission.READ_PHONE_STATE)
                .subscribe(new Consumer<Boolean>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        if (aBoolean) {
                            Constant.androidID = android.os.Build.MODEL;
                            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                            Constant.androidIMEI = telephonyManager.getDeviceId();
                        }
                    }
                });
    }
}
